/**
  StudioShell: An IDE built on Fitbit Studio
  Copyright (C) 2018 Adam Higerd

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.2
import QtQuick.Controls 1.1
import QtWebEngine 1.6
import com.alkahest.WindowIcon 1.0

ApplicationWindow {
  id: app
  visible: true
  x: initialX
  y: initialY
  width: initialWidth
  height: initialHeight
  title: webView.title
  WindowIcon.paths: [
    ":/images/icon1024.png",
    ":/images/icon512.png",
    ":/images/icon256.png",
    ":/images/icon128.png",
    ":/images/icon64.png",
    ":/images/icon32.png",
    ":/images/icon16.png",
  ]

  signal simulatorClicked()
  signal clearCookies()
  signal openPreferences()

  Timer {
    id: resetTimer
    interval: 500
    repeat: false
    onTriggered: {
      webView.reloadAndBypassCache();
    }
  }

  function handleCallback(method, args) {
    if (method == 'setWindowState') {
      setWindowState(args);
    } else if (method == 'logout') {
      clearCookies();
      resetTimer.start();
    } else {
      console.log(method, args);
    }
  }

  function runJavaScript(script) {
    return webView.runJavaScript(script);
  }

  function studioCall(method, args) {
    if (args) {
      webView.runJavaScript("window.studio." + method + "(...JSON.parse(" + JSON.stringify(args) + "))")
    } else {
      webView.runJavaScript("window.studio." + method + "()")
    }
  }

  menuBar: MenuBar {
    Menu {
      title: "&File"
      MenuItem {
        id: menuClose
        enabled: false
        text: "&Close Project"
        onTriggered: studioCall("closeProject")
      }
      MenuSeparator {}
      MenuItem {
        id: menuExport
        enabled: false
        text: "&Export Project"
        onTriggered: studioCall("exportProject")
      }
      MenuItem {
        id: menuPublish
        enabled: false
        text: "Build for Pu&blishing"
        onTriggered: studioCall("publishProject")
      }
      MenuSeparator {}
      MenuItem {
        text: "&Preferences..."
        onTriggered: app.openPreferences()
      }
      MenuSeparator {}
      MenuItem {
        text: "E&xit"
        onTriggered: Qt.quit()
      }
    }

    Menu {
      title: "&Develop"
      MenuItem {
        id: menuBuild
        enabled: false
        text: "&Build"
        shortcut: "Ctrl+B"
        onTriggered: studioCall("build")
      }
      MenuItem {
        id: menuRun
        enabled: false
        text: "&Run"
        shortcut: "Ctrl+R"
        onTriggered: studioCall("run")
      }
      MenuSeparator {}
      MenuItem {
        text: "Launch &Simulator"
        onTriggered: app.simulatorClicked()
      }
      MenuItem {
        text: "&Gallery App Manager"
        onTriggered: Qt.openUrlExternally("https://gam.fitbit.com/apps")
      }
    }

    Menu {
      title: "&Help"
      MenuItem {
        text: "&Developer Guide"
        onTriggered: Qt.openUrlExternally("https://dev.fitbit.com/build/guides/")
      }
      MenuItem {
        text: "API &Reference"
        onTriggered: Qt.openUrlExternally("https://dev.fitbit.com/build/reference/")
      }
      MenuSeparator {
        visible: Qt.platform.os != "osx"
      }
      MenuItem {
        text: "&About..."
        onTriggered: aboutDialog.visible = true
      }
    }
  }

  WebEngineView {
    id: webView
    anchors.fill: parent
    url: "https://studio.fitbit.com/"

    onTitleChanged: {
      app.title = webView.title;
    }

    onNewViewRequested: {
      if (request.requestedUrl == 'https://dev.fitbit.com/') {
        // The default help link is useless. Redirect to something better.
        Qt.openUrlExternally("https://dev.fitbit.com/build/guides/")
      } else {
        Qt.openUrlExternally(request.requestedUrl)
      }
    }

    userScripts: [
      WebEngineScript {
        injectionPoint: WebEngineScript.DocumentReady
        worldId: WebEngineScript.MainWorld
        sourceCode: "const css = document.createElement('STYLE'); css.innerHTML = '.hdr-menu, .ftr-social, .ftr-mail-me { display: none !important } .ftr-links { border-top: none !important }'; document.head.appendChild(css);"
      },
      WebEngineScript {
        injectionPoint: WebEngineScript.DocumentReady
        worldId: WebEngineScript.MainWorld
        sourceCode: initScript
      }
    ]
  }

  StudioShellAbout {
    id: aboutDialog
    WindowIcon.paths: [
      ":/images/icon1024.png",
      ":/images/icon512.png",
      ":/images/icon256.png",
      ":/images/icon128.png",
      ":/images/icon64.png",
      ":/images/icon32.png",
      ":/images/icon16.png",
    ]
  }

  function setWindowState(mode) {
    var projectIsOpen = (mode == 'editor-view');
    menuClose.enabled = projectIsOpen;
    menuExport.enabled = projectIsOpen;
    menuPublish.enabled = projectIsOpen;
    menuBuild.enabled = projectIsOpen;
    menuRun.enabled = projectIsOpen;
  }
}
