/**
  StudioShell: An IDE built on Fitbit Studio
  Copyright (C) 2018 Adam Higerd

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef PREFERENCES_H
#define PREFERENCES_H

#include <QDialog>
class QLineEdit;
class ShellUtils;

class PreferencesWindow : public QDialog {
  Q_OBJECT
public:
  PreferencesWindow(ShellUtils* utils, QWidget* parent = nullptr);

signals:
  void requestSimulatorDownload();

public slots:
  void save();

private slots:
  void findSimulator();

private:
  ShellUtils* utils;
  QLineEdit* simPathEdit;
};

#endif
