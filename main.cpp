/**
  StudioShell: An IDE built on Fitbit Studio
  Copyright (C) 2018 Adam Higerd

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <cstdlib>
#include <QFile>
#include <QApplication>
#include <QQmlApplicationEngine>
#include <QtQml/QQmlContext>
#include <QtWebEngine>
#include <QWebEngineUrlRequestInterceptor>
#include <QtWebEngineWidgets/QWebEngineProfile>
#include <QtWebEngineWidgets/QWebEngineSettings>
#include <QtWebEngineWidgets/QWebEngineDownloadItem>
#include <QMetaObject>
#include "shellutils.h"
#include "windowicon.h"

int main(int argc, char *argv[]) {
  setenv("QTWEBENGINE_CHROMIUM_FLAGS", "--remote-debugging-port=9900", true);

  QtWebEngine::initialize();
  QApplication app(argc, argv);
  app.setApplicationDisplayName("Studio Shell");
  app.setApplicationName("Studio Shell");
  app.setApplicationVersion("0.0.2");

  QIcon appIcon(":/images/icon1024.png");
  appIcon.addFile(":/images/icon512.png");
  appIcon.addFile(":/images/icon256.png");
  appIcon.addFile(":/images/icon128.png");
  appIcon.addFile(":/images/icon64.png");
  appIcon.addFile(":/images/icon32.png");
  appIcon.addFile(":/images/icon16.png");
  app.setWindowIcon(appIcon);

  QWebEngineSettings* settings = QWebEngineSettings::globalSettings();
  settings->setAttribute(QWebEngineSettings::JavascriptCanAccessClipboard, true);
  settings->setAttribute(QWebEngineSettings::ScrollAnimatorEnabled, true);
  settings->setAttribute(QWebEngineSettings::FullScreenSupportEnabled, true);

  QWebEngineProfile* profile = QWebEngineProfile::defaultProfile();
  profile->setPersistentCookiesPolicy(QWebEngineProfile::ForcePersistentCookies);
  QObject::connect(profile, &QWebEngineProfile::downloadRequested, [](QWebEngineDownloadItem* item) { item->accept(); });

  QQmlApplicationEngine engine;
  QQmlContext *context = engine.rootContext();

  qmlRegisterType<WindowIcon>("com.alkahest.WindowIcon", 1, 0, "WindowIcon");

  QRect geometry = QGuiApplication::primaryScreen()->availableGeometry();
  if (!QGuiApplication::styleHints()->showIsFullScreen()) {
    const QSize size = geometry.size() * 4 / 5;
    const QSize offset = (geometry.size() - size) / 2;
    const QPoint pos = geometry.topLeft() + QPoint(offset.width(), offset.height());
    geometry = QRect(pos, size);
  }
  context->setContextProperty("initialX", geometry.x());
  context->setContextProperty("initialY", geometry.y());
  context->setContextProperty("initialWidth", geometry.width());
  context->setContextProperty("initialHeight", geometry.height());

  QFile initScriptFile(":/studio.js");
  initScriptFile.open(QIODevice::ReadOnly);
  context->setContextProperty("initScript", QString::fromUtf8(initScriptFile.readAll()));

  engine.load("qrc:/main.qml");
  QObject* root = engine.rootObjects()[0];
  ShellUtils utils(profile, root);
  context->setContextProperty("shellUtils", &utils);
  return app.exec();
}
