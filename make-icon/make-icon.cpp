/**
  StudioShell: An IDE built on Fitbit Studio
  Copyright (C) 2018 Adam Higerd

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <QApplication>
#include <QLabel>
#include <QSlider>
#include <QHBoxLayout>
#include <QFormLayout>
#include <QPushButton>
#include <QImage>
#include <QPainter>
#include <math.h>
#include <QtDebug>

QSlider* makeSlider(QWidget* parent, double min, double max, double resolution, double value) {
  QSlider* slider = new QSlider(Qt::Horizontal, parent);
  slider->setMinimum(0);
  slider->setMaximum((max - min) / resolution);
  slider->setSingleStep(1);
  slider->setPageStep(max - min);
  slider->setValue((value - min) / resolution);
  QObject::connect(slider, SIGNAL(valueChanged(int)), parent, SLOT(updateSliders()));
  return slider;
}

class ImageMaker : public QWidget {
  Q_OBJECT
public slots:
  void updateSliders() {
    m_turns = (turnsSlider->value() * .02 + 1) * M_PI;
    m_exp = (expSlider->value() * .002 + .01);
    m_step = (stepSlider->value() * .002);
    m_scale = (scaleSlider->value() * .001 + .1);
    m_dot = (dotSlider->value() * .01 + .1);
    m_x = xSlider->value();
    m_y = ySlider->value();
    m_spread = spreadSlider->value() * .002 + 0.986;
    m_rot = (rotSlider->value() * .01) * M_PI * 2;
    qDebug() << m_turns << m_scale << m_exp << m_spread << m_step << m_dot << m_x << m_y << m_rot;
    preview->setPixmap(QPixmap::fromImage(makeImage(512)));
  }

  void save() {
    makeImage(1024).save("icon1024.png");
    makeImage(512).save("icon512.png");
    makeImage(256).save("icon256.png");
    makeImage(128).save("icon128.png");
    makeImage(64).save("icon64.png");
    makeImage(32).save("icon32.png");
    makeImage(16).save("icon16.png");
  }

public:
  QLabel* preview;
  QSlider *turnsSlider, *expSlider, *stepSlider, *dotSlider, *xSlider, *ySlider, *spreadSlider, *rotSlider, *scaleSlider;
  float m_turns, m_exp, m_step, m_dot, m_x, m_y, m_spread, m_rot, m_scale;

  ImageMaker() : QWidget(0) {
    QHBoxLayout* layout = new QHBoxLayout(this);
    layout->addWidget(preview = new QLabel(this));
    preview->setFixedSize(512, 512);

    QFormLayout* form = new QFormLayout;
    layout->addLayout(form);

    QPushButton* generateButton;
    form->addRow("Turns", turnsSlider = makeSlider(this, 1, 6, .02, 12.2523 / M_PI));
    form->addRow("Scale", scaleSlider = makeSlider(this, 0.1, 0.25, .001, .189));
    form->addRow("Exponent", expSlider = makeSlider(this, 0.01, 0.2, .002, .12));
    form->addRow("Spread", spreadSlider = makeSlider(this, 0.986, 1.1, .002, .992));
    form->addRow("Step", stepSlider = makeSlider(this, 0, 1, .002, .736));
    form->addRow("Dot Size", dotSlider = makeSlider(this, 0.1, 0.5, .01, .25));
    form->addRow("Center X", xSlider = makeSlider(this, 0, 1024, 1, 557));
    form->addRow("Center Y", ySlider = makeSlider(this, 0, 1024, 1, 593));
    form->addRow("Rotation", rotSlider = makeSlider(this, 0, 1, .01, 3.33009 / M_PI / 2));
    form->addRow(generateButton = new QPushButton("Generate", this));
    QObject::connect(generateButton, SIGNAL(clicked()), this, SLOT(save()));

    updateSliders();
  }

  QImage makeImage(int size) {
    QImage img(size, size, QImage::Format_ARGB32);
    img.fill(0);
    QPainter p(&img);
    p.setRenderHint(QPainter::Antialiasing, size >= 128);

    p.setPen(Qt::transparent);
    p.setBrush(QColor(0, 176, 185));
    p.drawEllipse(0, 0, size-1, size-1);

    p.setBrush(QColor(255, 255, 255));

    double xOff = m_x / 1024.8 * size;
    double yOff = m_y / 1024.8 * size;
    double endpoint = 2 * m_turns;
    double scale = size * m_scale / 8;
    for (double t = M_PI; t < endpoint + .2; t = (t * m_spread) + m_step) {
      double r = exp(m_exp * t) * scale;
      double x = cos(t + m_rot) * r + xOff;
      double y = -sin(t + m_rot) * r + yOff;
      double offset = r * m_dot;
      if (offset < .5) offset = .5;
      p.drawEllipse(x - offset, y - offset, 2 * offset, 2 * offset);
    }

    return img;
  }
};
#include <make-icon.moc>

int main(int argc, char** argv) {
  QApplication app(argc, argv);
  ImageMaker window;
  window.show();
  return app.exec();
}
