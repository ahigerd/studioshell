TEMPLATE = app
OBJECTS_DIR = .obj
MOC_DIR = .obj
RCC_DIR = .obj
win32: VERSION = 0.0.2.0
else: VERSION = 0.0.2

QT += qml quick webengine webenginewidgets widgets
DEFINES += QT_WEBVIEW_WEBENGINE_BACKEND
HEADERS += shellutils.h preferences.h windowicon.h
SOURCES += main.cpp shellutils.cpp preferences.cpp windowicon.cpp
RESOURCES += qml.qrc
ICON = images/fitbit.icns
