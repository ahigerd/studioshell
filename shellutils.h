/**
  StudioShell: An IDE built on Fitbit Studio
  Copyright (C) 2018 Adam Higerd

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef SHELLUTILS_H
#define SHELLUTILS_H

#include <QWebEngineUrlRequestInterceptor>
#include <QString>
#include <QVariant>

class QWebEngineProfile;

class ShellUtils : public QWebEngineUrlRequestInterceptor {
  Q_OBJECT
  Q_PROPERTY(QString simPath MEMBER simPath)
public:
  ShellUtils(QWebEngineProfile* profile, QObject* ui, QObject* parent = nullptr);
  ~ShellUtils();

  QString simPath;

  virtual void interceptRequest(QWebEngineUrlRequestInfo& info);

public slots:
  void saveSettings();
  void clearCookies();
  void launchSimulator();
  void openPreferences();
  void requestSimulatorDownload();

private:
  void invokeCallback(const QString& name, const QVariant& arg = QVariant());
  QWebEngineProfile* profile;
  QObject* ui;
  QWidget* prefsWindow;
};

#endif
