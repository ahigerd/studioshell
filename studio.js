/**
  StudioShell: An IDE built on Fitbit Studio
  Copyright (C) 2018 Adam Higerd

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
(() => {
  function click(...selectors) {
    return () => selectors.forEach(sel => document.querySelector(sel).click());
  }

  function hostCall(method, args) {
    // Intentionally consume and ignore the error, because this is expected to fail
    fetch(`https://callback/${method}?${args}`, { mode: 'no-cors' }).catch(() => {});
  }

  window.studio = {
    closeProject: click('button.pt-icon-cross'),
    exportProject: click('button.pt-icon-download', 'a.pt-icon-export'),
    publishProject: click('button.pt-icon-download', 'a.pt-icon-upload'),
    build: click('button.pt-icon-build'),
    run: click('a.pt-icon-play'),
  };

  function logout(event) {
    localStorage.clear();
    hostCall('logout');
    event.preventDefault();
    event.stopPropagation();
    setTimeout(() => window.location.href = 'https://studio.fitbit.com/login', 250);
  }

  let logoutHookInterval = false;
  function hookLogoutButton() {
    // I'm not sure why I have to be so aggressive in trying to find this button.
    const button = document.querySelector('.logout-button');
    if (button) {
      if (!button.isHooked) {
        button.isHooked = true;
        button.addEventListener('click', logout, true);
      }
      clearInterval(logoutHookInterval);
      logoutHookInterval = false;
    }
  }

  const windowStateObserver = new MutationObserver(() => {
    const container = document.querySelector('div.ui-container');
    const state = container ? container.className.replace('ui-container ', '') : 'loading';
    hostCall('setWindowState', state);
    if (logoutHookInterval) {
      clearInterval(logoutHookInterval);
    }
    if (state == 'projects-view') {
      logoutHookInterval = setInterval(hookLogoutButton, 100);
      hookLogoutButton();
    }
  });

  function observe() {
    const rootElement = document.querySelector('#root > span');
    if (rootElement) {
      windowStateObserver.observe(rootElement, { childList: true });
    } else {
      window.setTimeout(observe, 100);
    }
  }
  observe();
})()
