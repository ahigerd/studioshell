/**
  StudioShell: An IDE built on Fitbit Studio
  Copyright (C) 2018 Adam Higerd

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.2
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.3
import QtQuick.Window 2.2

ApplicationWindow {
  id: aboutDialog
  title: "About StudioShell"
  modality: Qt.platform.os == 'osx' ? Qt.NonModal : Qt.ApplicationModal
  flags: Qt.Dialog | Qt.CustomizeWindowHint | Qt.WindowSystemMenuHint | Qt.WindowTitleHint | Qt.WindowCloseButtonHint
  width: 400
  x: (Screen.width - 400) / 2
  y: (Screen.height - 400) / 3

  ColumnLayout {
    anchors.fill: parent
    Label {
      id: aboutLabel
      Layout.alignment: Qt.AlignCenter
      Layout.margins: 20
      width: 400
      textFormat: Text.StyledText
      horizontalAlignment: Text.AlignHCenter
      verticalAlignment: Text.AlignVCenter
      text: "<img src='qrc:/images/icon256.png' height='128' width='128' /><br/><br/>
        <font size='5'><b>StudioShell</b></font><br/>
        An integrated development environment for Fitbit\u00AE<br/>
        smartwatches built on top of Fitbit Studio<br/><br/>
        StudioShell \u00A9 2018 Adam Higerd, distributed under the <a href='http://www.gnu.org/licenses/lgpl.html'>GNU LGPL v3</a>.<br/>
        Fitbit Studio \u00A9 2018 <a href='https://www.fitbit.com'>Fitbit, Inc.</a>"

      onLinkActivated: Qt.openUrlExternally(link)
    }

    Button {
      focus: true
      visible: Qt.platform.os != 'osx'
      Layout.alignment: Qt.AlignCenter
      Layout.bottomMargin: 20
      text: "OK"
      onClicked: aboutDialog.close()
      Keys.onPressed: {
        if ((event.key == Qt.Key_W || event.key == Qt.Key_F4) && (event.modifiers & Qt.ControlModifier || event.modifiers & Qt.AltModifier)) {
          aboutDialog.close();
        }
      }
    }
  }
}
