#include "windowicon.h"
#include <QWindow>

WindowIconAttached* WindowIcon::qmlAttachedProperties(QObject* object)
{
  return new WindowIconAttached(object);
}

WindowIconAttached::WindowIconAttached(QObject* parent) : QObject(parent)
{
  // initializers only
}

void WindowIconAttached::setPaths(const QStringList& paths)
{
  QObject* ancestor = parent();
  QWindow* topLevel = nullptr;
  while (ancestor) {
    QWindow* window = qobject_cast<QWindow*>(ancestor);
    if (window) {
      topLevel = window;
    }
    ancestor = ancestor->parent();
  }
  if (!topLevel) {
    return;
  }
  QIcon icon;
  for (const QString& path : paths) {
    icon.addFile(path);
  }
  topLevel->setIcon(icon);
}
