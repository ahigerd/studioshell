/**
  StudioShell: An IDE built on Fitbit Studio
  Copyright (C) 2018 Adam Higerd

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "preferences.h"
#include "shellutils.h"
#include <QDesktopServices>
#include <QFormLayout>
#include <QGridLayout>
#include <QDialogButtonBox>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QFileDialog>
#include <QFile>

PreferencesWindow::PreferencesWindow(ShellUtils* utils, QWidget* parent)
: QDialog(parent), utils(utils) {
  setWindowTitle("StudioShell Preferences");
  QFormLayout* layout = new QFormLayout(this);

  QGridLayout* pathLayout = new QGridLayout();
  pathLayout->setContentsMargins(0, 0, 0, 0);
  pathLayout->setSpacing(2);
  pathLayout->addWidget(simPathEdit = new QLineEdit(this), 0, 0);
  simPathEdit->setMinimumWidth(300);
  simPathEdit->setText(utils->simPath);
  QPushButton* simPathButton = new QPushButton("...", this);
  pathLayout->addWidget(simPathButton, 0, 1);
  QLabel* downloadLink = new QLabel(" <a href='https://studio.fitbit.com/'>Download Fitbit OS Simulator</a>", this);
  pathLayout->addWidget(downloadLink, 1, 0, 1, 2);

  layout->addRow("Fitbit OS Simulator Path:", pathLayout);

  QDialogButtonBox* buttons = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel, this);
  layout->addRow(buttons);

  QObject::connect(simPathButton, SIGNAL(clicked()), this, SLOT(findSimulator()));
  QObject::connect(downloadLink, SIGNAL(linkActivated(QString)), this, SIGNAL(requestSimulatorDownload()));
  QObject::connect(buttons, SIGNAL(accepted()), this, SLOT(save()));
  QObject::connect(buttons, SIGNAL(rejected()), this, SLOT(reject()));
}

void PreferencesWindow::save() {
  utils->simPath = simPathEdit->text();
  utils->saveSettings();
  accept();
}

void PreferencesWindow::findSimulator() {
  QString path = QFileDialog::getOpenFileName(this, "Find Fitbit OS Simulator",
#ifdef Q_OS_MAC
    "/Applications/", "Applications (*.app)"
#elif Q_OS_WIN
    "C:\\Program Files (x86)\\", "Programs (*.exe *.cmd *.bat);;All Files (*.*)"
#else
    QStandardPaths::standardLocations(QStandardPaths::ApplicationsLocation)[0], "Programs (*.exe *.cmd *.bat);;All Files (*.*)"
#endif
  );
  if (!path.isEmpty() && QFile::exists(path)) {
    simPathEdit->setText(path);
  }
}
