# StudioShell

An integrated development environment for Fitbit� smartwatches built on top of [Fitbit Studio](https://studio.fitbit.com).

StudioShell � 2018 Adam Higerd, distributed under the [GNU LGPL v3](LICENSE.md).

Fitbit Studio � 2018 [Fitbit, Inc.](https://www.fitbit.com/)