/**
  StudioShell: An IDE built on Fitbit Studio
  Copyright (C) 2018 Adam Higerd

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "shellutils.h"
#include "preferences.h"
#include <QSettings>
#include <QProcess>
#include <QWebEngineProfile>
#include <QWebEngineCookieStore>
#include <QDesktopServices>
#include <QtDebug>

static bool isExternalUrl(const QUrl& url) {
  if (url.host() == "investor.fitbit.com") return true;
  if (url.host() == "community.fitbit.com") return true;
  if (url.host() == "dev.fitbit.com") return true;
  if (url.host() == "help.fitbit.com") return true;
  if (url.host() == "www.fitbit.com") {
    if (url.path().contains("/countries")) return false;
    if (url.path().contains("/login")) return false;
    if (url.path().contains("/oauth")) return false;
    return true;
  }
  return false;
}

ShellUtils::ShellUtils(QWebEngineProfile* profile, QObject* ui, QObject* parent)
: QWebEngineUrlRequestInterceptor(parent), profile(profile), ui(ui), prefsWindow(0) {
  profile->setRequestInterceptor(this);

  QObject::connect(ui, SIGNAL(clearCookies()), this, SLOT(clearCookies()));
  QObject::connect(ui, SIGNAL(openPreferences()), this, SLOT(openPreferences()));
  QObject::connect(ui, SIGNAL(simulatorClicked()), this, SLOT(launchSimulator()));

  QSettings settings;
  simPath = settings.value("simPath").toString();
}

ShellUtils::~ShellUtils() {
  if (prefsWindow) {
    delete prefsWindow;
  }
}

void ShellUtils::interceptRequest(QWebEngineUrlRequestInfo& info) {
  if (info.requestUrl().host() == "callback") {
    info.block(true);
    invokeCallback(info.requestUrl().path().mid(1), info.requestUrl().query());
  } else if (info.resourceType() == QWebEngineUrlRequestInfo::ResourceTypeMainFrame && isExternalUrl(info.requestUrl())) {
    // These links should be opened externally, not inside this application. They only ever happen on the intro and login pages.
    QDesktopServices::openUrl(info.requestUrl());
    info.redirect(QUrl("https://studio.fitbit.com/login"));
  }
}

void ShellUtils::clearCookies() {
  profile->clearHttpCache();
  profile->cookieStore()->deleteAllCookies();
  profile->cookieStore()->deleteSessionCookies();
}

void ShellUtils::saveSettings() {
  QSettings settings;
  settings.setValue("simPath", simPath);
  qDebug() << "saved" << simPath;
}

void ShellUtils::launchSimulator() {
  if (simPath.isEmpty()) {
    openPreferences();
  } else {
#if defined(Q_OS_WIN) || defined(Q_OS_MAC)
    QDesktopServices::openUrl("file://" + simPath);
#else
    QProcess::startDetached("wine", QStringList() << simPath);
#endif
  }
}

void ShellUtils::invokeCallback(const QString& name, const QVariant& arg) {
  QMetaObject::invokeMethod(ui, "handleCallback",
    Qt::QueuedConnection,
    Q_ARG(QVariant, name),
    Q_ARG(QVariant, arg)
  );
}

void ShellUtils::openPreferences() {
  if (!prefsWindow) {
    prefsWindow = new PreferencesWindow(this);
    QObject::connect(prefsWindow, SIGNAL(requestSimulatorDownload()), this, SLOT(requestSimulatorDownload()));
  }
  prefsWindow->show();
  prefsWindow->setFocus();
}

void ShellUtils::requestSimulatorDownload() {
#ifdef Q_OS_MAC
  QDesktopServices::openUrl(QUrl("https://simulator-updates.fitbit.com/download/latest/mac"));
#else
  QDesktopServices::openUrl(QUrl("https://simulator-updates.fitbit.com/download/latest/win"));
#endif
}
